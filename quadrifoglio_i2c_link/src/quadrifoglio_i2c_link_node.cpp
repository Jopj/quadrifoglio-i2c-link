#include "ros/ros.h"
#include "quadrifoglio_i2c_link/DebugMsg.h"
#include "quadrifoglio_i2c_link/EncoderMsg.h"
#include "quadrifoglio_i2c_link/ElectricMsg.h"

#include "quadrifoglio_i2c_link/messages_quadri.h"
#include "quadrifoglio_i2c_link/Transcutale_I2C_Master.hpp"

void FillFeedbackMessages(quadrifoglio_i2c_link::EncoderMsg &encMsg, quadrifoglio_i2c_link::ElectricMsg &eleMsg, FeedbackMsg &feedbackStruct){
    encMsg.frontLeft = feedbackStruct.frontLeftEncoder;
    encMsg.frontRight = feedbackStruct.frontRightEncoder;
    encMsg.rearLeft = feedbackStruct.rearLeftEncoder;
    encMsg.rearRight = feedbackStruct.rearRightEncoder;
    eleMsg.motorCurrent = ((float)feedbackStruct.motorCurrent)/1000;
    eleMsg.satelliteCurrent = ((float)feedbackStruct.raspiCurrent)/1000;
    eleMsg.servoCurrent = ((float)feedbackStruct.servoCurrent)/1000;
    eleMsg.ucCurrent = ((float)feedbackStruct.ucCurrent)/1000;
    eleMsg.batteryVoltage = ((float)feedbackStruct.batteryVoltage)/1000;
}

int main(int argc, char **argv){

    ros::init(argc, argv, "quadrifoglio_i2c_link_node");
    ros::NodeHandle n;
    ros::Publisher debugPublisher = n.advertise<quadrifoglio_i2c_link::DebugMsg>("i2c_link_debug", 1);
    ros::Publisher encoderPublisher = n.advertise<quadrifoglio_i2c_link::EncoderMsg>("i2c_link_encoders", 1);
    ros::Publisher electricPublisher = n.advertise<quadrifoglio_i2c_link::ElectricMsg>("i2c_link_electrics", 1);

    ros::Rate loop_rate(30);  //i2c communications, synced to ultrasound sensor module reading rate

    int counter = 0;
    quadrifoglio_i2c_link::DebugMsg debugMsg;
    quadrifoglio_i2c_link::EncoderMsg encoderMsg;
    quadrifoglio_i2c_link::ElectricMsg electricMsg;

    FeedbackMsg Feedback = {};	//Message from uc to raspi
    CommandMsg Command = {3000,3000,3000,3000,255,255,255,255,0,0};  //Message from raspi to uc

    int file_i2c = OpenI2CMaster(0x03);  //Main uc
    unsigned int debugCounter = 0;

    while(ros::ok()){
        //msg.num = counter;
        GetAnything(file_i2c, 1, Feedback);
        FillFeedbackMessages(encoderMsg, electricMsg, Feedback);
        SendAnything(file_i2c, 127, Command);
        
        debugMsg.num = debugCounter;
        debugPublisher.publish(debugMsg);
        encoderPublisher.publish(encoderMsg);
        electricPublisher.publish(electricMsg);

        if(!(counter%10)) ROS_INFO("Tink! %d", counter);
        debugCounter++;
        ros::spinOnce();
        loop_rate.sleep();
    }

}